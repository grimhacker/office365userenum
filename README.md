# Office 365 User Enum


WARNING: Microsoft appears to have fixed this issue around November 2019. I'm leaving the code here in case its of use to anyone in the future, but DO NOT expect it to work anymore!


Enumerate valid usernames from Office 365 using ActiveSync

Requires Python2.7.

Example usage:

> python2.7 office365userenum.py -u user_list -o output.txt



